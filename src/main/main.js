const fs = require("fs");
const parse = require("csv-parse/lib/sync");

// read data from files
const jsonBuffer = fs.readFileSync("src/main/resources/data.json");
JSONData = JSON.parse(jsonBuffer).devices;
let csvBuffer = fs.readFileSync("src/main/resources/data.csv");
var CSVData = parse(csvBuffer.toString(), { columns: true, cast: true });
let data = [...JSONData, ...CSVData];

// check for duplicates
const stringifiedData = data.map(row => JSON.stringify(row));
const distinctSet = new Set(stringifiedData);
data = [...distinctSet].map(row => JSON.parse(row));

// find devices in specified geo area
let devicesInArea = data
  .filter(row => row.lat <= 89.99 && row.lat > 0)
  .filter(row => row.long >= -179.99 && row.long < 0);

// find device with oldest update time
const oldestDevice = data.reduce((a, b) => (a.timestamp > b.timestamp ? a : b));

// find device with newest update time
const newestDevice = data.reduce((a, b) => (a.timestamp < b.timestamp ? a : b));

const result = {
  "Total Number of Devices": data.length,
  "Total Unique Devices": new Set(data.map(device => device.uuid)).size,
  "Devices within Geo Area": {
    "Total Number": devicesInArea.length,
    "Device IDs": devicesInArea.map(row => row.uuid)
  },
  "Device with Oldest Update Time": {
    uuid: oldestDevice.uuid,
    timestamp: oldestDevice.timestamp
  },
  "Device ID with Newest Update Time": {
    uuid: newestDevice.uuid,
    timestamp: newestDevice.timestamp
  }
};

// write to output file in prettified json format
fs.writeFileSync("results.json", JSON.stringify(result, null, 2));
console.log("Output sent to results.json");
